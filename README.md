# Florians Dotfiles

Unix dotfiles for easy setup of my personal environment

Run

    ./install.sh

to install basic software (like oh_my_zsh amd spacemacs).

Run

    ./setup.sh

to create symlinks to the included dotfiles.
