#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"

# Creating symlinks to dotfiles
FILES_TO_SYMLINK=(
    'shell/zshrc'
    'shell/profile'

    'git/gitconfig'
    'git/gitignore'

    'emacs/spacemacs'
)

for i in ${FILES_TO_SYMLINK[@]}; do
    if [[ -f ~/.${i##*/} ]]; then
        if [[ -L ~/.${i##*/} ]]; then
           rm ~/.${i##*/}
        else
            echo "Backing up ~/.${i##*/}"
            mv ~/.${i##*/} ~/.${i##*/}.bak
        fi
    fi
    ln -s $SCRIPT_DIR/$i ~/.${i##*/}
done

mkdir -p ~/.sbt/0.13/plugins/
rm ~/.sbt/0.13/plugins/plugins.sbt
ln -s ~/.sbt/0.13/plugins/plugins.sbt sbt/plugins.sbt
