sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install zsh emacs vim

mkdir ~/bin

# Install spacemacs
if [[ -d ~/.emacs.d ]]; then
    mv ~/.emacs.d ~/.emacs.d.bak
fi
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

# Install oh_my_zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "Please reboot to make zsh your default shell\n"
